import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';

import { CoreModule } from '../core/core.module';

import { ClinicsModule } from '../clinics/clinics.module';

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,

    CoreModule,

    ClinicsModule,
  ],
  declarations: [DashboardComponent]
})
export class DashboardModule { }
