import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard.component';

import { UserGuard } from '../user/shared/guards/user.guard';

import { routes as clinicRoutes } from '../clinics/clinics-routing.module';

const routes: Routes = [
  { path: 'dashboard',
    component: DashboardComponent,
    canActivate: [UserGuard],
    children: [
      { path: '', redirectTo: 'clinic', pathMatch: 'full'},
      {
        path: 'clinic',
        children: clinicRoutes
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
