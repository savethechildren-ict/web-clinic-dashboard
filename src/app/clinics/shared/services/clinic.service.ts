import { Injectable } from '@angular/core';

import { Clinic } from '../models/clinic';

import { AngularFireList,
         AngularFireObject,
         AngularFireDatabase,
         SnapshotAction} from 'angularfire2/database';

import { UploadService } from './upload.service';

import { Observable } from 'rxjs/Observable';

@Injectable()
export class ClinicService {
  basePath = '/v1/clinics';

  clinicsRef: AngularFireList<Clinic> = null;
  clinics$: Observable<SnapshotAction[]>;

  clinicRef: AngularFireObject<Clinic> = null;
  clinic$: Observable<SnapshotAction>;

  constructor(private db: AngularFireDatabase,
              private uploadService: UploadService) {}

  // Get a list of clinics
  getClinics$(): Observable<SnapshotAction[]> {
    if (this.clinicsRef === null) {
      this.clinicsRef = this.db.list<Clinic>(this.basePath);
      this.clinics$ = this.clinicsRef.snapshotChanges();
    }
    return this.clinics$;
  }

  // Return a single observable clinic
  getClinic$(key: string): Observable<SnapshotAction> {
    const clinicPath =  `${this.basePath}/${key}`;
    this.clinicRef = this.db.object(clinicPath);
    this.clinic$ = this.clinicRef.snapshotChanges();
    return this.clinic$;
  }

  // Create clinic
  createClinic(clinic: Clinic) {
    return this.clinicsRef.push(clinic);
  }

  // Update an existing clinic
  updateClinic(key: string, value: any) {
    this.clinicsRef.update(key, value)
      .catch(error => this.handleError(error));
  }
  // Deletes a single clinic
  deleteClinic(key: string): void {
    this.uploadService.getUploads(key)
      .first(uploads => uploads !== null)
      .subscribe(uploads => {
        const deleteArr = [];

        uploads.forEach(upload => {
          // console.log('Deleting: ' + upload.name);
          deleteArr.push(this.uploadService.deleteUpload(key, upload));
        });

        Promise.all(deleteArr)
          .then(() => {
            return this.clinicsRef.remove(key);
          })
          .catch(error => this.handleError(error));
      });
  }

  // Deletes the entire list of clinics
  /*
  deleteAll(): void {
    this.clinicsRef.remove()
      .catch(error => this.handleError(error));
  }
  */

  // Default error handling for all actions
  private handleError(error) {
    console.log(error);
  }

}
