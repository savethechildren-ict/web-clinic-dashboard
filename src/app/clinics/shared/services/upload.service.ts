import { Injectable } from '@angular/core';

import { Upload } from '../models/upload';

import * as firebase from 'firebase';
import { AngularFireDatabase } from 'angularfire2/database';

import { Observable } from 'rxjs/Observable';

@Injectable()
export class UploadService {
  basePath = '/v1/clinics';

  // uploadsRef: AngularFireList<Upload>;
  uploads$: Observable<Upload[]>;

  constructor(private db: AngularFireDatabase) { }

  getUploads(clinic: string) {
    this.uploads$ = this.db.list(`${this.basePath}/${clinic}/images/`).snapshotChanges().map((actions) => {
      return actions.map((a) => {
        const data = a.payload.val();
        const $key = a.payload.key;
        return { $key, ...data };
      });
    });
    return this.uploads$;
  }

  // Executes the file uploading to firebase https://firebase.google.com/docs/storage/web/upload-files
  pushUpload(clinic: string, upload: Upload) {
    const storageRef = firebase.storage().ref();
    const uploadTask = storageRef.child(`${this.basePath}/${clinic}/images/${upload.file.name}`).put(upload.file);

    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot: firebase.storage.UploadTaskSnapshot) =>  {
        // upload in progress
        const snap = snapshot;
        upload.progress = (snap.bytesTransferred / snap.totalBytes) * 100;
      },
      (error) => {
        // upload failed
        console.log(error);
      },
      () => {
        // upload success
        if (uploadTask.snapshot.downloadURL) {
          upload.url = uploadTask.snapshot.downloadURL;
          upload.name = upload.file.name;
          this.saveFileData(clinic, upload);
        } else {
          console.error('No download URL!');
        }
      },
    );
  }

  // Writes the file details to the realtime db
  private saveFileData(clinic: string, upload: Upload) {
    this.db.list(`${this.basePath}/${clinic}/images/`).push(upload);
  }

  deleteUpload(clinic: string, upload: Upload) {
    return this.deleteFileData(clinic, upload.$key)
      .then( () => {
        return this.deleteFileStorage(clinic, upload.name);
      })
      .catch((error) => console.log(error));
  }

  // Writes the file details to the realtime db
  private deleteFileData(clinic: string, key: string) {
    return this.db.list(`${this.basePath}/${clinic}/images/`).remove(key);
  }

  // Firebase files must have unique names in their respective storage dir
  // So the name serves as a unique key
  private deleteFileStorage(clinic: string, name: string) {
    const storageRef = firebase.storage().ref();
    return storageRef.child(`${this.basePath}/${clinic}/images/${name}`).delete();
  }
}
