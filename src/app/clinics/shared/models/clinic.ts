export class Clinic {
  institution: string;

  typeHygieneClinic: boolean;
  typeTreatmentHub: boolean;
  typeCondomStop: boolean;

  poc: ClinicPerson;

  address: string;
  city: string;
  province: string;
  region: string;

  lat: number;
  long: number;

  services: Array<ClinicService>;
  labtests: Array<ClinicService>;
  facilities: Array<ClinicFacility>;

  counselors: Array<ClinicPerson>;
  keys: Array<ClinicPerson>;
  doctors: Array<ClinicPerson>;
  educators: Array<ClinicPerson>;

  schedule: {
    sunday: ClinicSchedule;
    monday: ClinicSchedule;
    tuesday: ClinicSchedule;
    wednesday: ClinicSchedule;
    thursday: ClinicSchedule;
    friday: ClinicSchedule;
    saturday: ClinicSchedule;
  };

  email: string;
  numbers: Array<ClinicNumber>;

  [propName: string]: any;

  constructor() {
    this.institution = '';

    this.typeHygieneClinic = false;
    this.typeTreatmentHub = false;
    this.typeCondomStop = false;

    this.poc = new ClinicPerson();

    this.address = '';
    this.city = '';
    this.province = '';
    this.region = '';

    this.lat = 0;
    this.long = 0;

    this.services = [];
    this.labtests = [];
    this.facilities = [];

    this.counselors = [];
    this.keys = [];
    this.doctors = [];
    this.educators = [];

    this.schedule = {
      monday: new ClinicSchedule(true),
      tuesday: new ClinicSchedule(true),
      wednesday: new ClinicSchedule(true),
      thursday: new ClinicSchedule(true),
      friday: new ClinicSchedule(true),
      saturday: new ClinicSchedule(false),
      sunday: new ClinicSchedule(false),
    };

    this.email = '';
    this.numbers = [];
  }

  stringifyAddress() {
    return this.address + ', ' +
           this.city + ', ' +
           this.province + ', ' +
           this.region;
  }

  stringifyClinicServices() {
    return this.stringifyClinicServicesBase('services');
  }

  stringifyClinicLabtests() {
    return this.stringifyClinicServicesBase('labtests');
  }

  private stringifyClinicServicesBase(source) {
    if (this[source]) {
      return this[source].reduce((acc, curr) => {
        return acc += curr.name +
          ' (Php ' + curr.price + ') ' +
          (curr.details ? (': ' + curr.details) : '') +
          '\n';
      }, '');
    } else {
      return '';
    }
  }

  stringifyClinicFacilities() {
    if (this.facilities) {
      return this.facilities.reduce((acc, curr) => {
        return acc += curr.name +
          '\n';
      }, '');
    } else {
      return '';
    }
  }

  stringifyClinicCounselors() {
    return this.stringifyClinicPeopleBase('counselors');
  }

  stringifyClinicKeys() {
    return this.stringifyClinicPeopleBase('keys');
  }

  stringifyClinicDoctors() {
    return this.stringifyClinicPeopleBase('doctors');
  }

  stringifyClinicEducators() {
    return this.stringifyClinicPeopleBase('educators');
  }

  private stringifyClinicPeopleBase(source) {
    if (this[source]) {
      return this[source].reduce((acc, curr) => {
        return acc += curr.name +
          (curr.designation ? (' (' + curr.designation + ')') : '') +
          '\n';
      }, '');
    } else {
      return '';
    }
  }

  stringifyClinicSchedule() {
    if (this.schedule) {
      return this.stringifyDay('monday', this.schedule) + '\n' +
        this.stringifyDay('tuesday', this.schedule) + '\n' +
        this.stringifyDay('wednesday', this.schedule) + '\n' +
        this.stringifyDay('thursday', this.schedule) + '\n' +
        this.stringifyDay('friday', this.schedule) + '\n' +
        this.stringifyDay('saturday', this.schedule) + '\n' +
        this.stringifyDay('sunday', this.schedule);
    } else {
      return '';
    }
  }

  stringifyDay(literal, schedule) {
    return literal.charAt(0).toUpperCase() + literal.slice(1) +
      ' (' + (schedule[literal].open ?
          ('open): ' + (
              schedule[literal].start + ' - ' + schedule[literal].end
            ) + ')'
          ) :
          'closed)'
      );
  }

  stringifyClinicNumbers() {
    if (this.numbers) {
      return this.numbers.reduce((acc, curr) => {
        return acc += (curr.main ? '[Main] ' : '' ) +
          curr.type + ': ' +
          curr.number +
          (curr.ext ? (' ext. ' + curr.ext) : '') +
          (curr.details ? (' (' + curr.details + ')') : '') +
          '\n';
      }, '');
    } else {
      return '';
    }
  }
}

export function turnToClinic(obj: any): Clinic {
  return Object.assign(new Clinic(), obj);
}

export class ClinicService {
  name: string;
  price: number;
  details: string;

  constructor() {
    this.name = '';
    this.price = 0;
    this.details = '';
  }
}

export class ClinicFacility {
  name: string;

  constructor() {
    this.name = '';
  }
}

export class ClinicPerson {
  name: string;
  designation: string;

  constructor() {
    this.name = '';
    this.designation = '';
  }
}

export class ClinicImage {
  $key: string;
  url: string;
}
export class ClinicSchedule {
  open: boolean;
  start: string;
  end: string;

  constructor(weekday = true) {
    if (weekday) {
      this.open = true;
      this.start = '8:00';
      this.end = '17:00';
    } else {
      this.open = false;
      this.start = '8:00';
      this.end = '17:00';
    }
  }
}

export class ClinicNumber {
  main: boolean;
  type: string;
  number: string;
  ext: string;
  details: string;

  constructor() {
    this.main = true;
    this.type = CLINIC_NUMBER_TYPE[0];
    this.number = '';
    this.ext = '';
    this.details = '';
  }
}

export const CLINIC_NUMBER_TYPE = [
    'Landline'
  , 'Mobile'
];
