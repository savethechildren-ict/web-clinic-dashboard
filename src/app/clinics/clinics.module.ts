import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { ClinicsRoutingModule } from './clinics-routing.module';

import { ClinicListComponent } from './clinic-list/clinic-list.component';
import { ClinicDetailComponent } from './clinic-detail/clinic-detail.component';
import { ClinicFormComponent } from './clinic-form/clinic-form.component';

import { ClinicTypeFilterComponent } from './clinic-list/filters/clinic-type-filter.component';

import { ClinicImageListComponent } from './clinic-image-list/clinic-image-list.component';
import { ClinicImageDetailComponent } from './clinic-image-detail/clinic-image-detail.component';
import { ClinicImageFormComponent } from './clinic-image-form/clinic-image-form.component';

import { ClinicService } from './shared/services/clinic.service';
import { UploadService } from './shared/services/upload.service';

import { AngularFireDatabaseModule } from 'angularfire2/database';

import { ClarityModule } from 'clarity-angular';

@NgModule({
  imports: [
    CommonModule,
    ClinicsRoutingModule,

    ReactiveFormsModule,

    AngularFireDatabaseModule,

    ClarityModule
  ],
  declarations: [
    ClinicListComponent,
    ClinicDetailComponent,
    ClinicFormComponent,

    ClinicImageListComponent,
    ClinicImageDetailComponent,
    ClinicImageFormComponent,

    // Filters
    ClinicTypeFilterComponent
  ],
  providers: [
    ClinicService,
    UploadService
  ]
})
export class ClinicsModule { }
