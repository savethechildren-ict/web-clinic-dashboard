import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClinicListComponent } from './clinic-list/clinic-list.component';
import { ClinicDetailComponent } from './clinic-detail/clinic-detail.component';
import { ClinicFormComponent } from './clinic-form/clinic-form.component';

export const routes: Routes = [
  { path: '',
    children: [
      { path: '', redirectTo: 'list', pathMatch: 'full'},
      { path: 'list', component: ClinicListComponent },
      { path: 'detail/:id', component: ClinicDetailComponent },
      { path: 'new', component: ClinicFormComponent },
      { path: 'edit/:id', component: ClinicFormComponent }
    ]
  }
];

@NgModule({
  // imports: [RouterModule.forChild(routes)],
  imports: [RouterModule],
  exports: [RouterModule]
})
export class ClinicsRoutingModule { }
