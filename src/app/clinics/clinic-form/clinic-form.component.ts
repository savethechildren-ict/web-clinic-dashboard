import { Component, OnDestroy, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';

import { FormArray, FormControl, FormGroup, FormBuilder, Validators} from '@angular/forms';

import {
  Clinic,
  ClinicService,
  ClinicFacility,
  ClinicPerson,
  ClinicSchedule,
  ClinicNumber,
  CLINIC_NUMBER_TYPE
} from '../shared/models/clinic';
import { PHILIPPINE_PROVINCES } from '../../shared/data/provinces';
import { PHILIPPINE_REGIONS } from '../../shared/data/regions';
import { TIME_INTERVALS } from '../../shared/data/time_intervals';
import { ClinicService as ClinicHelperService } from '../shared/services/clinic.service';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

enum State {
  New = 0,
  Edit
}

@Component({
  selector: 'app-clinic-form',
  templateUrl: './clinic-form.component.html',
  styleUrls: ['./clinic-form.component.scss']
})
export class ClinicFormComponent implements OnInit, OnDestroy {
  routerSub: Subscription;
  clinicSub: Subscription;

  provinces = PHILIPPINE_PROVINCES;
  regions = PHILIPPINE_REGIONS;
  timeIntervals = TIME_INTERVALS;
  numberTypes = CLINIC_NUMBER_TYPE;

  state: State = State.New;

  clinic$: Observable<any>;
  clinic: any;

  clinicForm = this.formBuilder.group({
    institution: ['', Validators.required],

    // TODO: add validation that checks multiple controls
    typeHygieneClinic: [true],
    typeTreatmentHub: [false],
    typeCondomStop: [false],

    poc: this.initPerson(new ClinicPerson()),

    address: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    province: new FormControl('', Validators.required),
    region: new FormControl('', Validators.required),

    lat: new FormControl('', Validators.required),
    long: new FormControl('', Validators.required),

    services: this.formBuilder.array([]),
    labtests: this.formBuilder.array([]),
    facilities: this.formBuilder.array([]),

    counselors: this.formBuilder.array([]),
    keys: this.formBuilder.array([]),
    doctors: this.formBuilder.array([]),
    educators: this.formBuilder.array([]),

    schedule: this.formBuilder.group({
      monday: this.initSchedule(new ClinicSchedule(true)),
      tuesday: this.initSchedule(new ClinicSchedule(true)),
      wednesday: this.initSchedule(new ClinicSchedule(true)),
      thursday: this.initSchedule(new ClinicSchedule(true)),
      friday: this.initSchedule(new ClinicSchedule(true)),
      saturday: this.initSchedule(new ClinicSchedule(true)),
      sunday: this.initSchedule(new ClinicSchedule(true)),
    }),

    email: new FormControl('', [Validators.required, Validators.pattern(/^\S+@\S+$/)]),
    numbers: this.formBuilder.array([]),

  });

  constructor(
    private location: Location,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private clinicService: ClinicHelperService
  ) {}

  ngOnInit() {
    // TODO: set this.title depending on origin (Add, or Edit)
    // TODO: set this.clinic depending on origin (Add, or Edit)

    const path = this.location.path();

    if (path.startsWith('/dashboard/clinic/new')) {
      this.state = State.New;

      this.initFormWithClinic(this.clinicForm, new Clinic());

      // only for initial load
      this.clinicForm.get('typeHygieneClinic').setValue(true);
    } else if (path.startsWith('/dashboard/clinic/edit')) {
      this.state = State.Edit;

      this.routerSub = this.route.params.subscribe(params => {

        this.clinic$ = this.clinicService.getClinic$(params['id']);

        this.clinicSub = this.clinic$.subscribe(action => {
          this.clinic = {
            key: action.key,
            val: action.payload.val()
          };

          // initialize clinicForm
          this.reset(this.clinicForm);
          // this.initFormWithClinic(this.clinicForm, this.clinic.val);
        });
      });
    } else {
      // console.log('...why are you here? You are neither new nor edit!');
    }





  }

  initFormWithClinic(form: FormGroup, clinic: Clinic) {
    // update all 1st-level properties
    this.clinicForm.patchValue({
      institution: clinic.institution,

      typeHygieneClinic: clinic.typeHygieneClinic,
      typeTreatmentHub: clinic.typeTreatmentHub,
      typeCondomStop: clinic.typeCondomStop,

      address: clinic.address,
      city: clinic.city,
      province: clinic.province,
      region: clinic.region,

      lat: clinic.lat,
      long: clinic.long,

      email: clinic.email
    });

    // update: poc
    form.get('poc').setValue(clinic.poc);

    // update: services
    if (clinic.services) {
      const services = <FormArray>form.get('services');
      for (let i = 0; i < clinic.services.length; i++) {
        services.push(this.initService(clinic.services[i]));
      }
    }

    // update: labtests
    if (clinic.labtests) {
      const labtests = <FormArray>form.get('labtests');
      for (let i = 0; i < clinic.labtests.length; i++) {
        labtests.push(this.initService(clinic.labtests[i]));
      }
    }

    // update: facilities
    if (clinic.facilities) {
      const facilities = <FormArray>form.get('facilities');
      for (let i = 0; i < clinic.facilities.length; i++) {
        facilities.push(this.initFacility(clinic.facilities[i]));
      }
    }

    // update: counselors
    if (clinic.counselors) {
      const counselors = <FormArray>form.get('counselors');
      for (let i = 0; i < clinic.counselors.length; i++) {
        counselors.push(this.initPerson(clinic.counselors[i]));
      }
    }
    // update: keys
    if (clinic.keys) {
      const keys = <FormArray>form.get('keys');
      for (let i = 0; i < clinic.keys.length; i++) {
        keys.push(this.initPerson(clinic.keys[i]));
      }
    }

    // update: doctors
    if (clinic.doctors) {
      const doctors = <FormArray>form.get('doctors');
      for (let i = 0; i < clinic.doctors.length; i++) {
        doctors.push(this.initPerson(clinic.doctors[i]));
      }
    }

    // update: educators
    if (clinic.educators) {
      const educators = <FormArray>form.get('educators');
      for (let i = 0; i < clinic.educators.length; i++) {
        educators.push(this.initPerson(clinic.educators[i]));
      }
    }

    // update: weekdays
    form.get('schedule').get('monday').setValue(clinic.schedule.monday);
    form.get('schedule').get('tuesday').setValue(clinic.schedule.tuesday);
    form.get('schedule').get('wednesday').setValue(clinic.schedule.wednesday);
    form.get('schedule').get('thursday').setValue(clinic.schedule.thursday);
    form.get('schedule').get('friday').setValue(clinic.schedule.friday);
    form.get('schedule').get('saturday').setValue(clinic.schedule.saturday);
    form.get('schedule').get('sunday').setValue(clinic.schedule.sunday);

    // update: numbers
    if (clinic.numbers) {
      const numbers = <FormArray>form.get('numbers');
      for (let i = 0; i < clinic.numbers.length; i++) {
        numbers.push(this.initNumber(clinic.numbers[i]));
      }
    }
  }

  initPerson(person: ClinicPerson): FormGroup {
    return this.formBuilder.group({
      name: [person.name, Validators.required],
      designation: [person.designation],
    });
  }

  addPerson(key: string, form: FormGroup) {
    const control = <FormArray>form.get(key);
    control.push(this.initPerson(new ClinicPerson()));
  }

  initService(service: ClinicService): FormGroup {
    return this.formBuilder.group({
      name: [service.name, Validators.required],
      price: [service.price, [Validators.required, Validators.pattern(/^\d+$/)]],
      details: [service.details],
    });
  }

  addService(key: string, form: FormGroup) {
    const control = <FormArray>form.get(key);
    control.push(this.initService(new ClinicService()));
  }

  initFacility(facility: ClinicFacility): FormGroup {
    return this.formBuilder.group({
      name: [facility.name, Validators.required]
    });
  }

  addFacility(key: string, form: FormGroup) {
    const control = <FormArray>form.get(key);
    control.push(this.initFacility(new ClinicFacility()));
  }

  initSchedule(schedule: ClinicSchedule): FormGroup {
    return this.formBuilder.group({
      open: [schedule.open, Validators.required],
      start: [schedule.start, Validators.required],
      end: [schedule.end, Validators.required],
    });
  }

  addNumber(key: string, form: FormGroup) {
    const control = <FormArray>form.get(key);
    control.push(this.initNumber(new ClinicNumber()));
  }

  initNumber(number: ClinicNumber): FormGroup {
    return this.formBuilder.group({
      main: [number.main, Validators.required],
      type: [number.type, Validators.required],
      number: [number.number, Validators.required],
      ext: [number.ext],
      details: [number.details],
    });
  }

  removeItem(key: string, form: FormGroup, i: number) {
    const control = <FormArray>form.get(key);
    control.removeAt(i);
  }

  removeAllItems(key: string, form: FormGroup) {
    const control = (<FormArray>form.get(key));
    while (control.controls.length > 0) {
      control.removeAt(0);
    }
  }

  save(form: FormGroup) {
    switch (this.state) {
      case State.New:
        this.clinicService.createClinic(form.getRawValue());
        // TODO: show toast of add success
        break;
      case State.Edit:
        this.clinicService.updateClinic(this.clinic.key, form.getRawValue());
        // TODO: show toast of edit success
        break;
      default:
        // console.log('...why are you here? You are neither new nor edit!');
        break;
    }

    this.router.navigateByUrl('/');
  }

  reset(form: FormGroup) {
    // clear all 1st-level properties
    form.reset();

    // only for new Clinics
    if (this.state === State.New) {
      form.get('typeHygieneClinic').setValue(true);

      form.get('schedule').get('monday').setValue(new ClinicSchedule(true));
      form.get('schedule').get('tuesday').setValue(new ClinicSchedule(true));
      form.get('schedule').get('wednesday').setValue(new ClinicSchedule(true));
      form.get('schedule').get('thursday').setValue(new ClinicSchedule(true));
      form.get('schedule').get('friday').setValue(new ClinicSchedule(true));
      form.get('schedule').get('saturday').setValue(new ClinicSchedule(false));
      form.get('schedule').get('sunday').setValue(new ClinicSchedule(false));
    }

    // clear all: services
    this.removeAllItems('services', this.clinicForm);

    // clear all: labtests
    this.removeAllItems('labtests', this.clinicForm);

    // clear all: facilities
    this.removeAllItems('facilities', this.clinicForm);

    // clear all: counselors
    this.removeAllItems('counselors', this.clinicForm);

    // clear all: keys
    this.removeAllItems('keys', this.clinicForm);

    // clear all: doctors
    this.removeAllItems('doctors', this.clinicForm);

    // clear all: educators
    this.removeAllItems('educators', this.clinicForm);

    // clear all: numbers
    this.removeAllItems('numbers', this.clinicForm);

    // if State.Edit, initialize all other properties
    if (this.state === State.Edit) {
      this.initFormWithClinic(form, this.clinic.val);
    }
  }

  ngOnDestroy(): void {
    if (this.routerSub) { this.routerSub.unsubscribe(); }
    if (this.clinicSub) { this.clinicSub.unsubscribe(); }
  }
}
