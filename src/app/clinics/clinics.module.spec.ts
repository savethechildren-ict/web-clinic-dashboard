import { ClinicsModule } from './clinics.module';

describe('ClinicsModule', () => {
  let clinicsModule: ClinicsModule;

  beforeEach(() => {
    clinicsModule = new ClinicsModule();
  });

  it('should create an instance', () => {
    expect(clinicsModule).toBeTruthy();
  });
});
