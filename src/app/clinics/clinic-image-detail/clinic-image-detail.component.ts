import {Component, Input, OnInit} from '@angular/core';

import { Upload } from '../shared/models/upload';
import { UploadService } from '../shared/services/upload.service';

@Component({
  selector: 'app-clinic-image-detail',
  templateUrl: './clinic-image-detail.component.html',
  styleUrls: ['./clinic-image-detail.component.scss']
})
export class ClinicImageDetailComponent implements OnInit {
  @Input() upload: Upload;
  @Input() clinic: string;

  constructor(
    private uploadService: UploadService) { }

  ngOnInit() {
  }

  deleteUpload() {
    this.uploadService.deleteUpload(this.clinic, this.upload);
  }
}
