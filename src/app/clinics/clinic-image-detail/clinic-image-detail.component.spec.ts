import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicImageDetailComponent } from './clinic-image-detail.component';

describe('ClinicImageDetailComponent', () => {
  let component: ClinicImageDetailComponent;
  let fixture: ComponentFixture<ClinicImageDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClinicImageDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClinicImageDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
