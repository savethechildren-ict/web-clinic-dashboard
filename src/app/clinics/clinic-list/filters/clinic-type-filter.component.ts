import { Component, EventEmitter } from '@angular/core';
import { Filter } from 'clarity-angular';

@Component({
  selector: 'app-clinic-type-filter',
  template: `
    <clr-checkbox [clrChecked]="hygieneClinic"
                  (clrCheckedChange)="setHygieneClinic($event)">
      Hygiene Clinic
    </clr-checkbox>
    <clr-checkbox [clrChecked]="treatmentHub"
                  (clrCheckedChange)="setTreatmentHub($event)">
      Treatment Hub
    </clr-checkbox>
    <clr-checkbox [clrChecked]="condomStop"
                  (clrCheckedChange)="setCondomStop($event)">
      Condom Stop
    </clr-checkbox>
  `
})
export class ClinicTypeFilterComponent implements Filter<any> {
  hygieneClinic = false;
  treatmentHub = false;
  condomStop = false;

  changes: EventEmitter<any> = new EventEmitter<any>(false);

  accepts(clinic: any) {
    return (this.hygieneClinic && clinic.val.typeHygieneClinic) ||
      (this.treatmentHub && clinic.val.typeTreatmentHub) ||
      (this.condomStop && clinic.val.typeCondomStop);
  }

  isActive(): boolean {
    return this.hygieneClinic || this.treatmentHub || this.condomStop;
  }

  setHygieneClinic(value: any) {
    this.hygieneClinic = value;
    this.changes.emit(true);
  }

  setTreatmentHub(value: any) {
    this.treatmentHub = value;
    this.changes.emit(true);
  }

  setCondomStop(value: any) {
    this.condomStop = value;
    this.changes.emit(true);
  }
}
