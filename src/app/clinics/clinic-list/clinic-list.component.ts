import {Component, OnInit} from '@angular/core';

import { Clinic, turnToClinic } from '../shared/models/clinic';
import { ClinicService } from '../shared/services/clinic.service';

import { Angular2Csv } from 'angular2-csv/Angular2-csv';

import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-clinic-list',
  templateUrl: './clinic-list.component.html',
  styleUrls: ['./clinic-list.component.scss']
})
export class ClinicListComponent implements OnInit {
  clinics$: Observable<any[]>;
  selectedClinic = null;

  // clinics;
  turnToClinic = turnToClinic;

  constructor(private clinicService: ClinicService) { }

  ngOnInit() {
    this.clinics$ = this.clinicService.getClinics$()
      .map(changes => {
        return changes.map( c => {
          return {
            key: c.key,
            val: c.payload.val()
          };
        });
      });
  }

  deleteAll() {
    // this.clinicService.deleteAll();
  }

  viewClinic() {
    // console.log(this.selectedClinic.val);
    // console.log(this.selectedClinic.$key);
  }

  generateCSV() {
    this.clinics$
      .first()
      .subscribe(clinics => {
          const transformed = clinics.map( (clinic: Clinic) => {

            // Issue: Typescript casting using 'as' keyword not working as expected in Angular
            // Workaround: https://stackoverflow.com/questions/40062987/typescript-object-casting-not-working#
            const val = new Clinic();
            Object.assign(val, clinic.val);

            return {
              name: val.institution,
              type: (val.typeHygieneClinic ? 'Hygiene Clinic' : '') +
                    (val.typeTreatmentHub ? '\nTreatment Hub' : '') +
                    (val.typeCondomStop ? '\nCondom Stop' : ''),
              poc: val.poc.name + (val.poc.designation ? ` (${val.poc.designation})` : ''),
              address: val.stringifyAddress(),
              lat: val.lat,
              long: val.long,
              services: val.stringifyClinicServices(),
              labtests: val.stringifyClinicLabtests(),
              facilities: val.stringifyClinicFacilities(),
              counselors: val.stringifyClinicCounselors(),
              keys: val.stringifyClinicKeys(),
              doctors: val.stringifyClinicDoctors(),
              educators: val.stringifyClinicEducators(),
              schedule: val.stringifyClinicSchedule(),
              email: val.email,
              numbers: val.stringifyClinicNumbers()
            };
          });
          new Angular2Csv(transformed, 'SHC Clinics', {
            headers: [
              'Institution',
              'Type',
              'Point Of Contact',
              'Address',
              'Latitude',
              'Longitude',
              'Services',
              'Lab Tests',
              'Facilities',
              'Counselors',
              'Key Persons',
              'Doctors',
              'Educators',
              'Schedule',
              'Email',
              'Contact Numbers'
            ]
          });
      });

  }
}
