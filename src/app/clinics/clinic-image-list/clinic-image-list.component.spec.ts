import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicImageListComponent } from './clinic-image-list.component';

describe('ClinicImageListComponent', () => {
  let component: ClinicImageListComponent;
  let fixture: ComponentFixture<ClinicImageListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClinicImageListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClinicImageListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
