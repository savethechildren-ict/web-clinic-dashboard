import { Component, Input, OnInit } from '@angular/core';

import { Upload } from '../shared/models/upload';
import { UploadService } from '../shared/services/upload.service';

import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-clinic-image-list',
  templateUrl: './clinic-image-list.component.html',
  styleUrls: ['./clinic-image-list.component.scss']
})
export class ClinicImageListComponent implements OnInit {
  @Input() clinic: string;

  uploads: Observable<Upload[]>;

  constructor(private uploadService: UploadService) { }

  ngOnInit() {
    this.uploads = this.uploadService.getUploads(this.clinic);
  }

}
