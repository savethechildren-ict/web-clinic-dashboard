import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ClinicService } from '../shared/services/clinic.service';

import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-clinic-detail',
  templateUrl: './clinic-detail.component.html',
  styleUrls: ['./clinic-detail.component.scss']
})
export class ClinicDetailComponent implements OnInit {
  clinic$: Observable<any>;
  clinic: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private clinicService: ClinicService) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.clinic$ = this.clinicService.getClinic$(params['id']);

      this.clinic$.subscribe(action => {
        this.clinic = {
          key: action.key,
          val: action.payload.val()
        };
      });
    });

  }

  editClinic() {
    this.router.navigateByUrl('/dashboard/clinic/edit/' + this.clinic.key);
  }

  deleteClinic() {
    this.clinicService.deleteClinic(this.clinic.key);
    // TODO: raise confirmation; check if delete is succesfull
    this.router.navigateByUrl('/');
  }

}
