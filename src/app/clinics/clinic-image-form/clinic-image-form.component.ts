import { Component, Input, OnInit } from '@angular/core';

import { Upload } from '../shared/models/upload';

import { UploadService } from '../shared/services/upload.service';

@Component({
  selector: 'app-clinic-image-form',
  templateUrl: './clinic-image-form.component.html',
  styleUrls: ['./clinic-image-form.component.scss']
})
export class ClinicImageFormComponent implements OnInit {
  @Input() clinic: string;

  selectedFiles: FileList | null;
  currentUpload: Upload;

  constructor(private uploadService: UploadService) { }

  ngOnInit() {
  }

  detectFiles($event: Event) {
    this.selectedFiles = ($event.target as HTMLInputElement).files;
  }

  uploadMulti() {
    const files = this.selectedFiles;
    if (!files || files.length === 0) {
      console.error('No Files found!');
      return;
    }

    Array.from(files).forEach((file) => {
      this.currentUpload = new Upload(file);
      this.uploadService.pushUpload(this.clinic, this.currentUpload);
    });
  }
}
