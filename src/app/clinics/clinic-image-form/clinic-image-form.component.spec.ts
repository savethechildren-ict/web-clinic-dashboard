import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicImageFormComponent } from './clinic-image-form.component';

describe('ClinicImageFormComponent', () => {
  let component: ClinicImageFormComponent;
  let fixture: ComponentFixture<ClinicImageFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClinicImageFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClinicImageFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
