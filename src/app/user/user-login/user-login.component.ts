import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from '../shared/services/user.service';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.scss']
})
export class UserLoginComponent implements OnInit {
  valid: boolean = true;

  constructor(private router: Router,
              private userService: UserService) { }

  ngOnInit() {
    // if user is logged in, route to dashboard
    this.userService.isLoggedIn().subscribe(loggedIn => {
      if (loggedIn) {
        this.router.navigate(['']);
      }
    });
  }

  login(email, password) {
    this.userService.login(email, password)
      .then(() => {
        this.router.navigate(['']);
      })
      .catch(error => {
        console.log(error);
        this.valid = false;
      });
  }

  logout() {
    this.userService.logout();
  }

}
