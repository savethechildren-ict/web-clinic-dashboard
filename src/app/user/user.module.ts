import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserRoutingModule } from './user-routing.module';

import { SharedModule } from '../shared/shared.module';

import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { UserLoginComponent } from './user-login/user-login.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserListComponent } from './user-list/user-list.component';

import { UserService } from './shared/services/user.service';
import { UserGuard } from './shared/guards/user.guard';

@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,

    SharedModule,

    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  declarations: [
    UserLoginComponent,
    UserDetailComponent,
    UserListComponent
  ],
  providers: [
    UserService,
    UserGuard
  ]
})
export class UserModule { }
