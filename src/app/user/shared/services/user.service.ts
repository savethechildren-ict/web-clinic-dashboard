import { Injectable } from '@angular/core';

import * as firebase from 'firebase/app';
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';

@Injectable()
export class UserService {
  constructor(private router: Router,
              private auth: AngularFireAuth) {}

  getUserInfo() {
    return this.auth.authState.first();
  }

  isLoggedIn() {
    return this.auth.authState
      .first()
      .map(state => (state !== null ? true : false) );
  }

  login(email, password) {
    // this.auth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider());
    return this.auth.auth.signInWithEmailAndPassword(email, password);
  }
  logout() {
    this.auth.auth.signOut()
      .then(() => this.router.navigate(['user', 'login']));
  }

}
