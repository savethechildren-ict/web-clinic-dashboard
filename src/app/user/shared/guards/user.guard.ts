import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { UserService } from '../services/user.service';

@Injectable()
export class UserGuard implements CanActivate {

  constructor(public router: Router,
              public userService: UserService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    return this.userService.isLoggedIn()
      .first()
      .switchMap(value => {
        if (value) {
          return Observable.of(true);
        } else {
          this.router.navigate(['user', 'login']);
          return Observable.of(false);
        }
      });
  }
}
