import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

import { SharedModule } from './shared/shared.module';

import { UserModule } from './user/user.module';
import { DashboardModule } from './dashboard/dashboard.module';

import { AngularFireModule } from 'angularfire2';
import { environment } from '../environments/environment';

import { ClarityModule } from 'clarity-angular';

import 'rxjs/add/operator/first';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,

    AppRoutingModule,

    SharedModule,

    UserModule,
    DashboardModule,

    AngularFireModule.initializeApp(environment.firebase),

    ClarityModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
