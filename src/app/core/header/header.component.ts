import { Component, OnInit } from '@angular/core';
import { UserService } from '../../user/shared/services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  title = 'Clinic Dashboard';
  email = '';

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.getUserInfo().subscribe(value => this.email = value.email);
  }

  logout() {
    this.userService.logout();
  }

}
